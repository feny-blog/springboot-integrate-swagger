package com.liangfen.demo.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "测试用的API接口", tags = {"测试API接口"})
@RestController
public class DemoController {

    @ApiOperation(value = "获取Swagger描述", httpMethod = "GET")
    @GetMapping("/get")
    public String get(){
        return "Hello Swagger2";
    }
}
